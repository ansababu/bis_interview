<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/employee', 'EmployeeController@index')->name('employee');
Route::get('/employee-create', 'EmployeeController@create')->name('employee-create');
Route::get('/employee-insert', 'EmployeeController@insert')->name('employee-insert');
Route::get('/employee/update/{id}', 'EmployeeController@update')->name('employee/update');
Route::get('/employee-edit', 'EmployeeController@updateDetails')->name('employee-edit');
Route::post('/employee/delete/{id}', 'EmployeeController@destroy')->name('employee.destroy');
