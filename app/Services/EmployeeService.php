<?php

namespace App\Services;

use App\Models\Employees;
use App\Http\Repositories\EmployeeRepository;
use Illuminate\Database\DatabaseManager;
use Carbon\Carbon;

class EmployeeService
{
    private $db;
    private $employees;
    private $employeeRepo;

    public function __construct(
        DatabaseManager $db,
        Employees $employees,
        EmployeeRepository $employeeRepo
    ) {
        $this->db = $db;
        $this->employees = $employees;
        $this->employeeRepo =  $employeeRepo;
    }

    /**
     *getEmployeeDetails
     *
     * @return void
     */
    public function getEmployeeDetails()
    {
        return $this->employeeRepo->getAllEmployeeDetails();
    }

    /**
     * creating employee
     *
     * @param [type] $request
     */
    public function createEmployee($request)
    {
        $this->db->beginTransaction();
        $data = [
            'first_name' => $request->post('first_name'),
            'last_name' => $request->post('last_name'),
            'nationality' => $request->post('nationality'),
            'gender' => $request->post('gender'),
            'phone' => $request->post('phone'),
            'dob' => date('Y-m-d', strtotime($request->post('dob'))),
            'doj' => date('Y-m-d', strtotime($request->post('doj'))),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ];
        try {
            $employeeCreation = $this->employeeRepo->saveEmployee($data);

            $this->db->commit();

            return $employeeCreation;
        } catch (\Exception $e) {
            $this->db->rollback();

            return null;
        }
    }

    /**
     *getEmployeeDEtailsBasedId
     *
     * @param integer $id
     * @return employees
     */
    public function getUserDetails($id)
    {
       return $this->employeeRepo->getEmployeeDEtailsBasedId($id);
    }

    public function updateEmployee($request)
    {
        $data = [
            'first_name' => $request->post('first_name'),
            'last_name' => $request->post('last_name'),
            'nationality' => $request->post('nationality'),
            'gender' => $request->post('gender'),
            'phone' => $request->post('phone'),
            'dob' => date('Y-m-d', strtotime($request->post('dob'))),
            'doj' => date('Y-m-d', strtotime($request->post('doj'))),
            'updated_at' => Carbon::now()->toDateTimeString()

        ];
        $id = $request->post('id');

        return $this->employeeRepo->employeeUpdation($data, $id);
    }
}
