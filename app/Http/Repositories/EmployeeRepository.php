<?php

namespace App\Http\Repositories;

use Illuminate\Database\DatabaseManager;

use App\Models\Employees;

class EmployeeRepository
{
    private $db;
    private $employees;

    public function __construct(

        Employees $employees
    ) {

        $this->employees = $employees;
    }

    /**
     * getAllEmployeeDetails
     *
     * @return employees
     */
    public function getAllEmployeeDetails()
    {
        return $this->employees->select(['employees.*'])->paginate(5);
    }

    /**
     * saving employee
     *
     * @param  $data
     */
    public function saveEmployee($data)
    {
        return $this->employees->create($data);
    }

    /**
     *getEmployeeDEtailsBasedId
     *
     * @param integer $id
     * @return employees
     */
    public function getEmployeeDEtailsBasedId($id)
    {
        return $this->employees->where('id', $id)->get();
    }

    /**
     * employee Updation
     *
     * @param  $data
     * @param  $id
     */
    public function employeeUpdation($data, $id)
    {
        return $this->employees->where('id', $id)->update($data);
    }
}
