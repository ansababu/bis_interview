<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Repositories\EmployeeRepository;
use App\Services\EmployeeService;
use Illuminate\Database\DatabaseManager;
use Carbon\Carbon;
use Nexmo\Laravel\Facade\Nexmo;

class EmployeeController extends Controller
{
    private $db;
    private $employeeRepo;
    private $employeeService;


    public function __construct(
        DatabaseManager $db,
        EmployeeRepository $employeeRepo,
        EmployeeService $employeeService

    ) {
        $this->db = $db;
        $this->employeeRepo = $employeeRepo;
        $this->employeeService = $employeeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeDetails =   $this->employeeService->getEmployeeDetails();

        return view('employee.index', ['employeeDetails' => $employeeDetails]);
    }

    /**
     * Employee creation
     *
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * Employee insertion
     *
     * @param Request $request
     */
    public function insert(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);
//
//    if ($request->hasfile('image')) {
//             $imagePath = $request->file('image');
//             $imageName = $imagePath->getClientOriginalName();
//             $path = base_path() . '/public/uploads/events';
//             $request->file('image')->move($path, $imageName);
//             $myimage = 'public/uploads/events/' . $imageName;
//         }
//
        $employeeCreate = $this->employeeService->createEmployee($request);

        if ($employeeCreate) {
            Nexmo::message()->send([
                'to'   => $request->post('phone'),
                'from' => '0527961255',
                'text' => 'you have succesfully registered'
            ]);

            return redirect()->route('employee')
                ->with('success', 'Employees created successfully.');
        }
    }

    /**
     * fetching details of employee
     *
     * @param integer $id
     */
    public function update($id)
    {
        $employeeDetails = $this->employeeService->getUserDetails($id);

        return view('employee.edit', ['employeeDetails' => $employeeDetails]);
    }

    /**
     * employee updation
     *
     * @param Request $request
     */
    public function updateDetails(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'nationality' => 'required',
            'gender' => 'required'
        ]);

        $employeeUpdate = $this->employeeService->updateEmployee($request);

        if ($employeeUpdate) {
            return redirect()->route('employee')->with('success', 'EmployeeDetails updated successfully.');
        }
    }
    /**
     *Employee deletion
     *
     * @param Request $request
     * @param integer $id

     */
    public function destroy(Request $request, int $id)
    {
        $this->db->table('employees')->where('id', $id)->delete();

        return redirect()->route('employee')->with('success', 'EmployeeDetails deleted successfully.');
    }
}
