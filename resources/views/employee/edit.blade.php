@extends('home')

@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="mb-4">
            <strong>Employee Updation</strong>
        </h4>
        <form method="get" action="{{ route('employee-edit') }}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @csrf
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">First Name</label>
                <div class="col-md-6">
                    <input type="text" name="first_name" class="form-control" id="first_name" value="{{$employeeDetails[0]->first_name}}" required>
                    <input type="hidden" name="id" class="form-control" name="id" value="{{$employeeDetails[0]->id}}">

                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">Last Name</label>
                <div class="col-md-6">
                <input type="text" name="last_name" class="form-control" id="last_name" value="{{$employeeDetails[0]->last_name}}" required>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">Nationality</label>
                <div class="col-md-6">
                   <select name="nationality" class="form-control">
                   <option value="">Select</option> 
                       <option value="indian" @if($employeeDetails[0]->nationality == 'indian') selected="selected" @endif>Indian</option>
                       <option value="America" @if($employeeDetails[0]->nationality == 'America') selected="selected" @endif >America</option>
                   </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseEmail">DOB</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="dob" placeholder="DOB" id="dob" value="{{date('m/d/Y', strtotime($employeeDetails[0]->dob))}}" data-toggle="datetimepicker" data-target="#datepicker" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseEmail">DOJ</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="doj" placeholder="DOJ" id="doj" data-toggle="datetimepicker" value="{{date('m/d/Y', strtotime($employeeDetails[0]->doj))}}" data-target="#end-datepicker" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">Gender</label>
                <div class="col-md-6">
                   <select name="gender" class="form-control">
                   <option value="">Select</option> 
                       <option value="male" @if($employeeDetails[0]->gender == 'male') selected="selected" @endif >male</option>
                       <option value="female" @if($employeeDetails[0]->gender == 'female') selected="selected" @endif >female</option>
                   </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseEmail">phone</label>
                <div class="col-md-6">
                <input type="tel" id="phone" name="phone" class="form-control" pattern="[1-9]{3}[0-9]{9}" title="Phone number with country code and remaing 9 digit with 0-9" value="{{$employeeDetails[0]->phone}}" required>
                </div>
            </div>
            <button class="btn btn-success px-5" type="submit">submit</button>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
  

  $('#dob').datepicker({
            uiLibrary: 'bootstrap4'
        });
        $('#doj').datepicker({
            uiLibrary: 'bootstrap4'
        });
         $(".join-group").click(function() {
        // $(".join-group").html("Requested");

        var groupId = $("#group_ids").val();
        var userId = $("#user_ids").val();

        if (typeof(userId) == 'undefined' || userId == '') {
            const url = location.origin + '/meet-up';
            swal({
                title: 'Please login',
                text: 'Login and continue',
                icon: 'warning',
                //buttons: ["Cancel", "Yes!"],
                confirmButtonText: "Ok",
                cancelButtonText: "Cancel",
            }).then(function(value) {
                if (value) {
                    window.location.href = url;
                }
            });
        } else {
            $.ajax({
                url: location.origin + "/group-members",
                method: 'GET',
                data: {
                    "_token": "{{ csrf_token() }}",
                    group_id: groupId,
                    user_id: userId
                },

                success: function(data) {
                    //console.log(data);
                    if (data.message == "success") {
                        location.reload();
                        $(".join-group").hide();
                        $(".dropdown").show();
                        var url = "{{$url_1}}";
                        var id = data.result;
                        var url1 = url + id + "&group_id=" + groupId;
                        $(".leave").attr("href", url1);

                    }

                }
            });
        } 
        }); 

        function imgToData(input) {
            if ($('images').length < 15) {
                var div = document.getElementById("upload_images_div");
                if (input.files) {
                    var length = input.files.length;
                    if (length <= 15) {
                        document.getElementById('upload_images_div').innerHTML = "";
                        $.each(input.files, function(i, v) {
                            var n = i + 1;
                            var File = new FileReader();
                            File.onload = function(event) {
                                $('<img/>').attr({
                                    src: event.target.result,
                                    class: 'ad_img',
                                    id: 'ad_img-' + n + '-preview',
                                }).appendTo(div);
                            };
                            File.readAsDataURL(input.files[i]);
                        });
                    } else {
                        document.getElementById('file').value = '';
                        alert('{{__("text.place_ad_slogan_8")}}');
                    }
                }
            } else {
                document.getElementById('file').value = '';
                alert('{{__("text.place_ad_slogan_8")}}');
            }
        }

        $('input[type="file"]').change(function(event) {
            imgToData(this);
        });
         $(".comment-container").delegate(".edit", "click", function() {

        var well = $(this).parent().parent();
        var cid = $(this).attr("cid");
        var name = $(this).attr('name_a');
        var token = $(this).attr('token');
        var groupId = $("#group_ids").val();
        var commentDetails = $(this).attr("data-comment");

        var commentUrl = "{{$url_comment}}" + '/update-comment';
        str = commentDetails.toString();

        var form = '<form method="post" action=' + commentUrl + '><input type="hidden" name="_token" value="' + token + '"><input type="hidden" name="user_id" value="' + userId + '"><input type="hidden" name="comment_id" value="' + cid + '"><input type="hidden" name="group_id" value="' + groupId + '"><input type="hidden" name="name" value="' + name + '"><div class="form-group"><textarea class="form-control" name="reply" placeholder="Enter your reply" >' + commentDetails + ' </textarea> </div> <div class="form-group"> <input class="btn btn-primary" type="submit"> </div></form>';


        $('.details-comment-' + cid).append(form);

        // well.find(".reply-form").append(form);



    });   
   
</script>
@endsection
