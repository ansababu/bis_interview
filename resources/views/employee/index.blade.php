@extends('home')

@section('content')
<div class="card">
    <div class="card-body">
        <p><a href="{{ route('employee-create') }}" class="btn btn-success btn-square">Add New Employee</a></p>
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-5">
                    <table class="table table-hover nowrap" id="example1">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th> First Name</th>
                                <th> Last Name</th>
                                <th> Gender</th>
                                <th> DOB</th>
                                <th> DOJ</th>
                                <th> phone</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $id = 0; ?>
                            @foreach( $employeeDetails as $employeeDetail)
                            <tr>
                                <td>{{ ++$id }}</td>
                                <td>{{ $employeeDetail->first_name}}</td>
                                <td>{{ $employeeDetail->last_name}}</td>
                                <td>{{ $employeeDetail->gender}}</td>
                                <td>{{ date('d-m-Y', strtotime($employeeDetail->dob)) }}</td>
                                <td>{{ date('d-m-Y', strtotime($employeeDetail->doj)) }}</td>
                                <td>{{ $employeeDetail->phone}}</td>
                                <td>
                                    <a href="{{ url('/employee/update/'. $employeeDetail->id) }}" class="btn btn-block btn-primary">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('employee.destroy', $employeeDetail->id ) }}" method="POST">
                                      
                                        @csrf
                                        <button class="btn btn-block btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>


                            @endforeach
                        </tbody>

                    </table>
                    {!! $employeeDetails->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection