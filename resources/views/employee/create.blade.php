@extends('home')

@section('content')

<div class="card">
    <div class="card-body">
        <h4 class="mb-4">
            <strong>Employee Creation</strong>
        </h4>
        <form method="get" action="{{ route('employee-insert') }}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @csrf
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">First Name</label>
                <div class="col-md-6">
                    <input type="text" name="first_name" class="form-control" id="first_name" required>
                   

                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">Last Name</label>
                <div class="col-md-6">
                <input type="text" name="last_name" class="form-control" id="last_name" required>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">Nationality</label>
                <div class="col-md-6">
                   <select name="nationality" class="form-control">
                   <option value="">Select</option> 
                       <option value="indian">Indian</option>
                       <option value="America">America</option>
                   </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseEmail">DOB</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="dob" placeholder="DOB" id="dob" data-toggle="datetimepicker" data-target="#datepicker" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseEmail">DOJ</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="doj" placeholder="DOJ" id="doj" data-toggle="datetimepicker" data-target="#end-datepicker" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseFullname">Gender</label>
                <div class="col-md-6">
                   <select name="gender" class="form-control">
                   <option value="">Select</option> 
                       <option value="male">male</option>
                       <option value="female">female</option>
                   </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="baseEmail">phone</label>
                <div class="col-md-6">
                <input type="tel" id="phone" name="phone" pattern="[1-9]{3}[0-9]{9}" title="Phone number with country code and remaing 9 digit with 0-9" class="form-control" required>
                </div>
            </div>
            <button class="btn btn-success px-5" type="submit">submit</button>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
  

  $('#dob').datepicker({
            uiLibrary: 'bootstrap4'
        });
        $('#doj').datepicker({
            uiLibrary: 'bootstrap4'
        });      
   
</script>
@endsection